Python >= 3.6 is required

---

Before executing the project for the first time, you need to execute this commands:

- pip install -r requirements.txt
- python manage.py makemigrations
- python manage.py migrate

To create a super user please execute **python manage.py createsuperuser** and enter the required information. After that you can log in at http://localhost:8000/admin

---

To run the project execute **python manage.py runserver**. By default it will run on **localhost:8000**

---

To load dummy data run **python manage.py load_dummy_data**