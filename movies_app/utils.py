from django.core.paginator import Paginator
from django.db.models import Value
from django.db.models.functions import Concat
from django.http import JsonResponse

from movies_app.models import Movie
from movies_backend.settings import MEDIA_URL


def get_movies_response(request, queryset=None):
    if queryset is None:
        queryset = Movie.objects.all()

    movies = queryset.annotate(
        preview_image_url=Concat(Value(MEDIA_URL), 'preview_image')
    )

    try:
        page = int(request.GET.get('page', '0'))
    except:
        page = 0
    paginator = Paginator(movies, 12)
    paginator_page = paginator.page(page + 1).object_list
    return JsonResponse({
        'movies': list(paginator_page.values('title', 'slug', 'preview_image_url', 'summary')),
        'total_movies': movies.count()
    })
