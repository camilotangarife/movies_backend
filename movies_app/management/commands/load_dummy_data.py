import json
import os

import requests
from django.core.files.base import ContentFile
from django.core.management import BaseCommand
from django.utils.text import slugify

from movies_app.models import Movie
from movies_backend.settings import BASE_DIR


class Command(BaseCommand):
    help = 'Create dummy data'

    def handle(self, *args, **options):
        print('Loading dummy data')
        with open(os.path.join(BASE_DIR, 'movies_app', 'management', 'commands', 'dummy_data.json')) as json_file:
            movies = json.load(json_file)
            for m in movies:
                exist = Movie.objects.filter(title__iexact=m['title']).first()
                if not exist:
                    preview_image = 'https://placehold.it/182x268?text=' + m['title']
                    image = 'https://placehold.it/510x755?text=' + m['title']
                    slug = slugify(m['title'])
                    movie = Movie(
                        title=m['title'],
                        slug=slug,
                        year=m['year'],
                        summary=m['summary'],
                        director=m['director'],
                        writers=m['writers'],
                        stars=m['stars']
                    )
                    images_name = slug + '.png'
                    movie.image.save('preview-'+images_name, _download_image(image), save=False)
                    movie.preview_image.save('image-'+images_name, _download_image(preview_image), save=False)

                    movie.save()

        print('Finish')


def _download_image(url):
    response = requests.get(url)
    if response.status_code == 200:
        return ContentFile(response.content)
    return None