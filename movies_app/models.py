from django.contrib.auth.models import User
from django.db import models


class AppUser(models.Model):
    fk_user = models.OneToOneField(User, on_delete=models.PROTECT, verbose_name='System user')
    birthdate = models.DateField(verbose_name='Birthdate')
    address = models.TextField(verbose_name='Address')

    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Date created')
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Date updated')

    class Meta:
        verbose_name = 'App user'
        verbose_name_plural = 'App users'

    def __str__(self):
        return self.fk_user.email


class Movie(models.Model):
    title = models.CharField(max_length=100, unique=True, verbose_name='Title')
    slug = models.SlugField(max_length=100, unique=True, verbose_name='Slug')
    preview_image = models.ImageField(upload_to='movies-images', verbose_name='Preview image')
    image = models.ImageField(upload_to='movies-images', verbose_name='Image')
    year = models.PositiveIntegerField(verbose_name='Year', default=0)
    summary = models.TextField(verbose_name='Summary')

    director = models.CharField(max_length=100, default='', verbose_name='Director', blank=True)
    writers = models.CharField(max_length=100, default='', verbose_name='Writers', blank=True)
    stars = models.CharField(max_length=100, default='', verbose_name='Stars', blank=True)
    trailer = models.URLField(
        max_length=100,
        default='',
        verbose_name='Trailer',
        blank=True,
        help_text='Please insert the youtube url for iframe tags'
    )

    created_by = models.ForeignKey(AppUser, on_delete=models.SET_NULL, verbose_name='Created by', null=True, blank=True)
    date_created = models.DateTimeField(auto_now_add=True, verbose_name='Date created')
    date_updated = models.DateTimeField(auto_now=True, verbose_name='Date updated')

    class Meta:
        ordering = ('-year', 'title')
        verbose_name = 'Movie'
        verbose_name_plural = 'Movies'

    def __str__(self):
        return self.title