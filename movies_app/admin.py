from django.contrib import admin
from django.contrib.admin import AdminSite

from movies_app.models import AppUser, Movie


class MovieAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'slug', 'year', 'date_created', 'date_updated')
    list_display_links = ('id', 'title', 'slug')
    search_fields = ('title', 'summary', 'director', 'writers')
    fieldsets = (
        ('Main info', {
            'fields': ('title', 'slug', 'year', 'summary')
        }),
        ('images', {
            'fields': ('preview_image', 'image')
        }),
        ('Additional info', {
            'classes': ('collapse',),
            'fields': ('director', 'writers', 'stars', 'trailer'),
        }),
        ('Change control', {
            'classes': ('collapse',),
            'fields': ('created_by', 'date_created', 'date_updated'),
        }),
    )
    readonly_fields = ('date_created', 'date_updated')
    prepopulated_fields = {
        'slug': ('title',)
    }


class AppUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'fk_user', 'date_created', 'date_updated')
    list_display_links = ('id', 'fk_user')
    list_filter = ('birthdate',)
    readonly_fields = ('date_created', 'date_updated')


admin.site.register(Movie, MovieAdmin)
admin.site.register(AppUser, AppUserAdmin)

admin.site.site_title = 'OmnMovies'
admin.site.index_title = 'Movies Backend administration'
