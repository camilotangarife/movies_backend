import json

from dateutil.parser import parse
from django.contrib.auth import logout

from django.contrib.auth.models import User
from django.db.transaction import atomic
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import DeletionMixin
from jwt_auth.mixins import JSONWebTokenAuthMixin

from movies_app.models import Movie, AppUser
from movies_app.utils import get_movies_response


class MoviesView(View):
    def get(self, request):
        return get_movies_response(request=request)


class SuggestedMoviesView(JSONWebTokenAuthMixin, View):
    def get(self, request):
        app_user: AppUser = AppUser.objects.filter(fk_user=request.user).first()
        if app_user:
            movies = Movie.objects.filter(
                year__gte=app_user.birthdate.year + 10,
                year__lte=app_user.birthdate.year + 40
            )
            return get_movies_response(request=request, queryset=movies)
        else:
            return JsonResponse({'error': 'The user does not exist'}, status=401)


@method_decorator(csrf_exempt, name='dispatch')
class MyMoviesView(JSONWebTokenAuthMixin, View):
    def get(self, request):
        app_user: AppUser = AppUser.objects.filter(fk_user=request.user).first()
        if app_user:
            movies = Movie.objects.filter(created_by=app_user)
            return get_movies_response(request=request, queryset=movies)
        else:
            return JsonResponse({'error': 'The user does not exist'}, status=401)

    @atomic
    def post(self, request):
        app_user: AppUser = AppUser.objects.filter(fk_user=request.user).first()
        if app_user:

            title = request.POST.get('title', '')
            year_str = request.POST.get('year', '0')
            summary = request.POST.get('summary', '')
            director = request.POST.get('director', '')
            writers = request.POST.get('writers', '')
            stars = request.POST.get('stars', '')
            preview_image = request.FILES.get('preview_image', None)
            image = request.FILES.get('image', None)

            try:
                year = int(year_str)
            except:
                year = 0

            exist = Movie.objects.filter(title__iexact=title).first()
            if not exist:
                slug = slugify(title)
                movie = Movie(
                    title=title,
                    slug=slug,
                    year=year,
                    summary=summary,
                    director=director,
                    writers=writers,
                    stars=stars,
                    preview_image=preview_image,
                    image=image,
                    created_by=app_user
                )
                movie.save()

            return JsonResponse({
                'message': 'Your movie was created successfully'
            }, status=200)
        else:
            return JsonResponse({'error': 'The user does not exist'}, status=401)


class DeleteMovieView(JSONWebTokenAuthMixin, DeletionMixin, View):
    @atomic
    def delete(self, request, *args, **kwargs):
        slug = kwargs.get('slug', '')
        movie = Movie.objects.filter(slug=slug, created_by__fk_user=request.user).first()
        if movie:
            movie.delete()
            return JsonResponse({'message': 'works'})
        else:
            return JsonResponse({'error': 'You cannot delete this movie because it was not created by you'}, status=401)


class MovieDetails(View):
    def get(self, request, slug):
        movie = Movie.objects.filter(slug=slug).first()
        if movie:
            return JsonResponse({
                'title': movie.title,
                'slug': movie.slug,
                'summary': movie.summary,
                'image_url': movie.image.url if movie.image else '',
                'director': movie.director,
                'writers': movie.writers,
                'stars': movie.stars,
                'trailer': movie.trailer,
                'year': movie.year
            })
        else:
            return JsonResponse({
                'error': 'The movie does not exist'
            }, status=404)
    def delete(self, request):
        pass

@method_decorator(csrf_exempt, name='dispatch')
class SignupView(View):
    @atomic
    def post(self, request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        email = body.get('email', '')
        password = body.get('password', '')
        password2 = body.get('password2', '')
        name = body.get('name', '')
        lastname = body.get('lastname', '')
        birthdate_str = body.get('birthdate', '')
        address = body.get('address', '')

        if email and password:
            exist = AppUser.objects.filter(fk_user__username=email).first()
            if not exist:
                if name and lastname and birthdate_str and address:
                    if password == password2:
                        user = User(
                            username=email,
                            email=email,
                            first_name=name,
                            last_name=lastname
                        )
                        user.set_password(raw_password=password)
                        user.save()
                        app_user = AppUser(
                            fk_user=user,
                            birthdate=parse(birthdate_str),
                            address=address
                        )
                        app_user.save()

                        return JsonResponse({
                            'message': 'You have signed up succesfully. Now you can log in'
                        }, status=200)
                    else:
                        error = f'Passwords are not the same'
                else:
                    error = 'All fields are required'
            else:
                error = f'An user with the e-mail {email} is already registered'
        else:
            error = 'Please enter your email and password to sign up'

        return JsonResponse({
            'error': error
        }, status=400)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return JsonResponse({'response': True})
