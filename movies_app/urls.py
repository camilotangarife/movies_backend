from django.urls import path
from jwt_auth.views import obtain_jwt_token

from movies_app.views import MoviesView, MovieDetails, SignupView, SuggestedMoviesView, LogoutView, MyMoviesView, \
    DeleteMovieView

urlpatterns = [
    path('movies', MoviesView.as_view(), name='movies'),
    path('movies/<slug:slug>', MovieDetails.as_view(), name='movie-details'),
    path('suggested-movies', SuggestedMoviesView.as_view(), name='suggested-movies'),
    path('my-movies', MyMoviesView.as_view(), name='my-movies'),
    path('my-movies/delete/<slug:slug>', DeleteMovieView.as_view(), name='delete-movie'),
    path('login', obtain_jwt_token),
    path('logout', LogoutView.as_view(), name='logout'),
    path('signup', SignupView.as_view(), name='signup')
]
